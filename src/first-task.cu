/*
 ============================================================================
 Name        : first-task.cu
 Author      : Georgiy Odisharia
 Version     :
 Copyright   :
 Description : CUDA compute reciprocals
 ============================================================================
 */

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <chrono>
#include <iostream>
#include <numeric>
#include <stdlib.h>
#include <stdio.h>

static void _free() {
	if (::hostData != nullptr)
		std::free((void *)::hostData);

	if (::hostBuffer != nullptr)
		std::free((void *)::hostBuffer);

	if (::devData != nullptr)
		cudaFree((void *)::devData);

	if (::devBuffer != nullptr)
		cudaFree((void *)::devBuffer);
}

static void CheckCudaErrorAux (const char *, unsigned, const char *, cudaError_t);
#define CUDA_CHECK_RETURN(value) CheckCudaErrorAux(__FILE__,__LINE__, #value, value)

/**
 * Check the return value of the CUDA runtime API call and exit
 * the application if the call has failed.
 */
static void CheckCudaErrorAux (const char *file, unsigned line, const char *statement, cudaError_t err)
{
	if (err == cudaSuccess)
		return;
	std::cerr << statement<<" returned " << cudaGetErrorString(err) << "("<<err<< ") at "<<file<<":"<<line << std::endl;
	exit (1);
}

static const int LENGTH	= 10;
static const int TIME 	= 5;
static float DISCRETIZATION_LENGTH = 0.05;
static float DISCRETIZATION_TIME = 0.001;

/**
 * CUDA kernel that computes needed datum
 */
__global__ void shaftKernel(float *data, float *cache, const size_t vectorSize,
										const unsigned timeSize, const float DISCRETIZATION_TIME,
										const float DISCRETIZATION_LENGTH){
	unsigned idx = blockIdx.x*blockDim.x+threadIdx.x;

	for (unsigned i = 0; i < timeSize; i++){
		cache[vectorSize - 1] = data[vectorSize - 1] + 5;
		cache[0] = 0;

		if ( (idx < vectorSize - 1) && (idx > 0) ) {
			cache[idx] = (data[idx + 1] + data[idx - 1] - 2 * data[idx]) * DISCRETIZATION_TIME /
				(DISCRETIZATION_LENGTH * DISCRETIZATION_LENGTH) + data[idx];
		}
		__syncthreads();
		if (idx < vectorSize){
			data[idx] = cache[idx];
		}
		__syncthreads();

	}
}

__global__ void shaftKernelRestricted(float * __restrict__ data, float * __restrict__ cache,
	const size_t vectorSize, const unsigned timeSize,
	const float DISCRETIZATION_TIME, const float DISCRETIZATION_LENGTH){
	unsigned idx = blockIdx.x*blockDim.x+threadIdx.x;

	for (unsigned i = 0; i < timeSize; i++){
		if ( (idx < vectorSize - 1) && (idx > 0) ) {
			cache[idx] = (data[idx + 1] + data[idx - 1] - 2 * data[idx]) * DISCRETIZATION_TIME /
				(DISCRETIZATION_LENGTH * DISCRETIZATION_LENGTH) + data[idx];
		}
		__syncthreads();
		if (idx < vectorSize){
			data[idx] = cache[idx];
		}
		__syncthreads();

	}
}

/**
 * CPU parallel work
 */
#pragma omp parallel
void cpuShaft(float *data, float *cache, const size_t vectorSize,
										const unsigned timeSize, const float DISCRETIZATION_TIME,
										const float DISCRETIZATION_LENGTH){
#pragma omp for
	for (unsigned i = 0; i < timeSize; i++){
		cache[vectorSize - 1] = data[vectorSize - 1] + 5;
		cache[0] = 0;
#pragma omp for
		for (unsigned i = 1; i < vectorSize - 1; i++){
			cache[idx] = (data[idx + 1] + data[idx - 1] - 2 * data[idx]) * DISCRETIZATION_TIME /
				(DISCRETIZATION_LENGTH * DISCRETIZATION_LENGTH) + data[idx];
		}
#pragma omp for
		for (unsigned i = 0; i < vectorSize; i++){
			data[idx] = cache[idx];
		}
	}
}

/**
 * Host function that copies the data and launches the work on GPU
 */
float *gpuPrepare(float *data, unsigned size, unsigned int times){
	float *rc = new float[size];
	float *gpuData, *gpuCache;

	CUDA_CHECK_RETURN(cudaMalloc((void **)&gpuData, sizeof(float)*size));
	CUDA_CHECK_RETURN(cudaMalloc((void **)&gpuCache, sizeof(float)*size));
	CUDA_CHECK_RETURN(cudaMemcpy(gpuData, data, sizeof(float)*size, cudaMemcpyHostToDevice));

	static const int BLOCK_SIZE = 256;
	const int blockCount = (size+BLOCK_SIZE-1)/BLOCK_SIZE;
	shaftKernel<<<blockCount, BLOCK_SIZE>>> (gpuData, gpuCache, size, times, DISCRETIZATION_TIME, DISCRETIZATION_LENGTH);

	cudaError_t cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess){
		printf("Error!\n");
	}

	CUDA_CHECK_RETURN(cudaMemcpy(rc, gpuData, sizeof(float)*size, cudaMemcpyDeviceToHost));
	CUDA_CHECK_RETURN(cudaFree(gpuData));
	CUDA_CHECK_RETURN(cudaFree(gpuCache));
	return rc;
}



void initialize(float *data, unsigned size)
{
	for (unsigned i = 0; i < size; ++i)
		data[i] = 0;
}

int main(void)
{
	static int WORK_SIZE 	= LENGTH / DISCRETIZATION_LENGTH + 1;			// work size
	static int WORK_TIMES 	= TIME / DISCRETIZATION_TIME;						// work times

	float timeGPU;

	// init datum
	float *data 		= malloc (WORK_SIZE * sizeof(*data));
	float *cpuData 	= malloc (WORK_SIZE * sizeof(*cpuData));
	float *cpuCache = malloc (WORK_SIZE * sizeof(*cpuCache));
	std::cout << WORK_SIZE << "\n";
	initialize (data, WORK_SIZE);
	initialize (cpuData, WORK_SIZE);

	// run on gpu
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start, 0);
	float *result = gpuPrepare(data, WORK_SIZE, WORK_TIMES);
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&timeGPU, start, stop);
	cudaEventDestroy(start);
	cudaEventDestroy(stop);

	// run on cpu
	auto beginTime = std::chrono::steady_clock::now();
	cpuShaft(cpuData, cpuCache, WORK_SIZE, WORK_TIMES, DISCRETIZATION_TIME, DISCRETIZATION_LENGTH);
	auto chronoTimeCPU = std::chrono::duration_cast<std::chrono::microseconds>
											(std::chrono::steady_clock::now() - beginTime).count();

	for (unsigned i = 0; i < WORK_SIZE; i++) {
		printf("CPU returned: %f, ", cpuData[i]);
		printf("GPU returned: %f\n", result[i]);
	}

	// gnuplot
  FILE * gnuplotPipe = popen ("gnuplot -persistent", "w");

	fprintf(gnuplotPipe, "plot '-' \n");
	for (int i = 0; i < WORK_SIZE; i++)
	{
		fprintf(gnuplotPipe, "%lf %lf\n", i * DISCRETIZATION_LENGTH, result[i]);
		fprintf(gnuplotPipe, "%lf %lf\n", i * DISCRETIZATION_LENGTH, cpuData[i]);
	}

	fprintf(gnuplotPipe, "e");

	std::cout << std::setw(20) << std::left << "CPU time " << chronoTimeCPU   << " us" << std::endl;
	std::cout << std::setw(20) << std::left << "GPU time " << timeGPU	   << " ms" << std::endl;
	// std::cout << std::setw(20) << std::left << "GPU optimized time " << timeGPUOpt << "(" << chronoTimeGPUOp << " us by chrono) ms" << std::endl;

	/*
	 * Memory free
	 */

	cudaCheck(cudaFree((void *)devData));
	cudaCheck(cudaFree((void *)devBuffer));

	_free();

	system("pause");

	return 0;
}


	/* Free memory */
	free(data);
	free(result);

	return 0;
}


